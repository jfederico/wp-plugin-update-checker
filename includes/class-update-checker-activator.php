<?php

/**
 * Fired during plugin activation
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Update_Checker
 * @subpackage Update_Checker/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1.0
 * @package    Update_Checker
 * @subpackage Update_Checker/includes
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Update_Checker_Activator
{
    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    0.1.0
     */
    public static function activate()
    {
    }
}
