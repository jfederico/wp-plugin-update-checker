<?php

/**
 * Register all actions and filters for the plugin
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Update_Checker
 * @subpackage Update_Checker/includes
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Update_Checker
 * @subpackage Update_Checker/includes
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Update_Checker_Loader
{
    /**
     * The array of actions registered with WordPress.
     *
     * @since    0.1.0
     * @access   protected
     * @var      array    $actions    The actions registered with WordPress to fire when the plugin loads.
     */
    protected $actions;

    /**
     * The array of filters registered with WordPress.
     *
     * @since    0.1.0
     * @access   protected
     * @var      array    $filters    The filters registered with WordPress to fire when the plugin loads.
     */
    protected $filters;

    protected $checkers;

    /**
     * Initialize the collections used to maintain the actions and filters.
     *
     * @since    0.1.0
     */
    public function __construct()
    {
        $this->actions = array();
        $this->filters = array();
        $this->checkers = array();
    }

    /**
     * Add a new action to the collection to be registered with WordPress.
     *
     * @since    0.1.0
     * @param    string               $hook             The name of the WordPress action that is being registered.
     * @param    object               $component        A reference to the instance of the object on which the action is defined.
     * @param    string               $callback         The name of the function definition on the $component.
     * @param    int                  $priority         Optional. The priority at which the function should be fired. Default is 10.
     * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1.
     */
    public function add_action($hook, $component, $callback, $priority = 10, $accepted_args = 1)
    {
        $this->actions = $this->add($this->actions, $hook, $component, $callback, $priority, $accepted_args);
    }

    /**
     * Add a new filter to the collection to be registered with WordPress.
     *
     * @since    0.1.0
     * @param    string               $hook             The name of the WordPress filter that is being registered.
     * @param    object               $component        A reference to the instance of the object on which the filter is defined.
     * @param    string               $callback         The name of the function definition on the $component.
     * @param    int                  $priority         Optional. The priority at which the function should be fired. Default is 10.
     * @param    int                  $accepted_args    Optional. The number of arguments that should be passed to the $callback. Default is 1
     */
    public function add_filter($hook, $component, $callback, $priority = 10, $accepted_args = 1)
    {
        $this->filters = $this->add($this->filters, $hook, $component, $callback, $priority, $accepted_args);
    }

    /**
     * A utility function that is used to register the actions and hooks into a single
     * collection.
     *
     * @since    0.1.0
     * @access   private
     * @param    array                $hooks            The collection of hooks that is being registered (that is, actions or filters).
     * @param    string               $hook             The name of the WordPress filter that is being registered.
     * @param    object               $component        A reference to the instance of the object on which the filter is defined.
     * @param    string               $callback         The name of the function definition on the $component.
     * @param    int                  $priority         The priority at which the function should be fired.
     * @param    int                  $accepted_args    The number of arguments that should be passed to the $callback.
     * @return   array                                  The collection of actions and filters registered with WordPress.
     */
    private function add($hooks, $hook, $component, $callback, $priority, $accepted_args)
    {
        $hooks[] = array(
            'hook'          => $hook,
            'component'     => $component,
            'callback'      => $callback,
            'priority'      => $priority,
            'accepted_args' => $accepted_args
        );

        return $hooks;
    }

    /**
     * Register the filters and actions with WordPress.
     *
     * @since    0.1.0
     */
    public function run()
    {
        foreach ($this->filters as $hook) {
            add_filter($hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args']);
        }

        foreach ($this->actions as $hook) {
            add_action($hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args']);
        }

        // TODO: Replace constant with actual settings.
        /**
         *  In wp-config.php add:
         *    define('CLOUD3DOTS_UPDATE_CHECKER', <json_document>);
         *  Where <json_document> looks like:
         *    {
         *      "lodgexyz": {
         *      "root": "themes/lodgexyz",
         *      "repo":"https://gitlab.com/cloud3dots/wp-theme-lodgexyz/",
         *      "token":"xxxxxxxxxxxxxxxx",
         *      "branch":"master"}
         *    }
         *
         *  For Plugins the root should include the main file.
        **/
        $components_enabled = array();
        if (defined('CLOUD3DOTS_UPDATE_CHECKER')) {
            $components_enabled = json_decode(CLOUD3DOTS_UPDATE_CHECKER, true);
        } else {
            $components_enabled = json_decode(get_option('update_checker_components_enabled'), true);
        }
        foreach ($components_enabled as $component_slug => $component) {
            $fullPath = ABSPATH . 'wp-content/' . $component['root'];
            $this->checkers[$component_slug] = Puc_v4_Factory::buildUpdateChecker(
                $component['repo'],
                $fullPath,
                $component_slug
            );

            // Optional: If you're using a private repository, specify the access token like this:
            if (isset($component['token'])) {
                $this->checkers[$component_slug]->setAuthentication($component['token']);
            }

            // Optional: Set the branch that contains the stable release.
            if (isset($component['branch'])) {
                $this->checkers[$component_slug]->setBranch($component['branch']);
            }
        }
    }
}
