<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       cloud3dots.com
 * @since      0.1.0
 *
 * @package    Update_Checker
 * @subpackage Update_Checker/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Update_Checker
 * @subpackage Update_Checker/admin
 * @author     cloud3dots <cloud3dots@gmail.com>
 */
class Update_Checker_Admin
{
    /**
     * The ID of this plugin.
     *
     * @since    0.1.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    0.1.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * The options name to be used in this plugin
     *
     * @since  	0.1.0
     * @access 	private
     * @var  	string 		$option_name 	Option name of this plugin
     */
    private $option_name = 'update_checker';

    /**
     * Initialize the class and set its properties.
     *
     * @since    0.1.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    0.1.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Update_Checker_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Update_Checker_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/update-checker-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    0.1.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Update_Checker_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Update_Checker_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/update-checker-admin.js', array( 'jquery' ), $this->version, false);
    }

    /**
     * Add an options page under the Settings submenu
     *
     * @since  0.1.0
     */
    public function add_options_page()
    {
        $this->plugin_screen_hook_suffix = add_options_page(
            __('Update Checker', 'update-checker'),
            __('Update Checker', 'update-checker'),
            'manage_options',
            $this->plugin_name,
            array( $this, 'display_options_page' )
            );
    }

    /**
     * Render the options page for plugin
     *
     * @since  0.1.0
     */
    public function display_options_page()
    {
        include_once 'partials/update-checker-admin-display.php';
    }

    /**
     * Register the options for this plugin
     *
     * @since  0.1.0
     */
    public function register_setting()
    {
        // Add a General section
        add_settings_section(
            $this->option_name . '_general',
            __('General', 'update-checker'),
            array( $this, $this->option_name . '_general_cb' ),
            $this->plugin_name
        );

        add_settings_field(
            $this->option_name . '_enable_all',
            __('Enable all', 'update-checker'),
            array( $this, $this->option_name . '_enable_all_cb' ),
            $this->plugin_name,
            $this->option_name . '_general',
            array( 'label_for' => $this->option_name . '_enable_all' )
        );

        register_setting($this->plugin_name, $this->option_name . '_enable_all', array( $this, $this->option_name . '_sanitize_enable_all' ));

        add_settings_field(
            $this->option_name . '_components_enabled',
            __('Components enabled', 'update-checker'),
            array( $this, $this->option_name . '_components_enabled_cb' ),
            $this->plugin_name,
            $this->option_name . '_general',
            array( 'label_for' => $this->option_name . '_components_enabled' )
        );

        register_setting($this->plugin_name, $this->option_name . '_components_enabled', array( $this, $this->option_name . '_sanitize_components_enabled' ));
    }

    /**
     * Render the text for the general section
     *
     * @since  0.1.0
     */
    public function update_checker_general_cb()
    {
        echo '<p>' . __('Please change the settings accordingly.', 'update-checker') . '</p>';
    }

    /**
     * Render the check input field for enable_all option
     *
     * @since  0.1.0
     */
    public function update_checker_enable_all_cb()
    {
        $enable_all = get_option($this->option_name . '_enable_all'); ?>
            <fieldset>
              <label>
                <input type="checkbox" name="<?php echo $this->option_name; ?>_enable_all" id="<?php echo $this->option_name; ?>_enable_all" value="1" <?php checked($enable_all, 1, true); ?> />
              </label>
            </fieldset>
        <?php
    }

    /**
     * Render the textarea input field for components_enabled option
     *
     * @since  0.1.0
     */
    public function update_checker_components_enabled_cb()
    {
        $components_enabled = get_option($this->option_name . '_components_enabled'); ?>
            <fieldset>
              <label>
                <textarea type='textarea' name="<?php echo $this->option_name; ?>_components_enabled" id="<?php echo $this->_components_enabled; ?>_components_enabled"
                  rows="6" cols="60" /><?php echo $components_enabled; ?></textarea>
              </label>
            </fieldset>
        <?php
    }

    /**
     * Sanitize the text enable_all value before being saved to database
     *
     * @param  string $enable_all $_POST value
     * @since  0.1.0
     * @return string           Sanitized value
     */
    public function update_checker_sanitize_enable_all($enable_all)
    {
        return $enable_all;
    }

    /**
     * Sanitize the text enable_all value before being saved to database
     *
     * @param  string $components_enabled $_POST value
     * @since  0.1.0
     * @return string           Sanitized value
     */
    public function update_checker_sanitize_components_enabled($components_enabled)
    {
        return $components_enabled;
    }

    /**
     * Add setting link to plugin page.
     * Applied to the list of links to display on the plugins page (beside the activate/deactivate links).
     *
     * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
     *
     * @param array $links
     *
     * @return array
     */
    public function plugin_action_links($links)
    {
        $links[] = '<a href="'. esc_url(get_admin_url(null, 'options-general.php?page=update-checker')) .'">' . esc_html__('Settings', 'github-updater') . '</a>';
        return $links;
    }

    public function appended_action_links($links, $plugin_file)
    {
        $action = esc_html__('Enable Update Checker', 'github-updater');
        if ($this->plugin_enabled($plugin_file)) {
            $action = esc_html__('Disable Update Checker', 'github-updater');
        }
        $links[] = '<a href="'. esc_url(get_admin_url(null, "options-general.php?page=update-checker&component=$plugin_file")) .'">' . $action . '</a>';
        return $links;
    }

    public function get_all_plugins()
    {
        // Check if get_plugins() function exists. This is required on the front end of the
        // site, since it is in a file that is normally only loaded in the admin.
        if (! function_exists('get_plugins')) {
            require_once ABSPATH . 'wp-admin/includes/plugin.php';
        }
        return get_plugins();
    }

    public function plugin_enabled($plugin_file)
    {
        $components_enabled = json_decode(get_option($this->option_name . '_components_enabled'), true);
        if (!is_array($components_enabled)) {
            return false;
        }
        $plugin_parts = explode("/", $plugin_file);
        return (bool) array_key_exists($plugin_parts[0], $components_enabled);
    }
}
